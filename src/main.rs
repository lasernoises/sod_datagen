extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;
extern crate serde_json;

use std::fs::File;
use std::io::prelude::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Part {
    behaviour: u8,
    force_threshold: f64,
    rectangles: Vec<([u32; 2], [u32; 2])>,
    data_connectors: Vec<[f32; 2]>,
    physical_connectors: Vec<([f32; 2], f32)>
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 3 {
        let mut input = File::open(&args[1]).unwrap();
        let mut output = File::create(&args[2]).unwrap();

        let part: Part = serde_json::from_reader(&mut input).unwrap();
        rmps::encode::write(&mut output, &part).unwrap();
    }
}
